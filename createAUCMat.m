function [ oneSidedAUC, validTest ] = createAUCMat( turnTestResults )
%CREATEAUCMAT finds area under the curve values for each neuron
%   Given the analysis previously run (turnTestResults), calculates
%   the area under the curve value to quantify discriminability. The one-sided AUC is Choice
%   Probability
%
% Written by Jake Olson - April 2017

multiTest = 0;
if iscell(turnTestResults.validTest) && all(all(cellfun(@numel,turnTestResults.validTest)<=1))
    validTest = cell2mat(turnTestResults.validTest);
    auc = cell2mat(turnTestResults.auc);
elseif ~iscell(turnTestResults.validTest)
    validTest = turnTestResults.validTest;
    auc = turnTestResults.auc;
else
    multiTest = 1;
end

if ~multiTest
    if size(validTest,1)==1
        validTest = validTest'; % Data is in column vector format
    end
    for iNeu = 1:size(validTest,1)
        for jTest = 1:size(validTest,2)
            if ~validTest(iNeu,jTest)
                oneSidedAUC{iNeu,jTest} = NaN;
                nEachSpot(iNeu,jTest) = 0;
            else
                oneSidedAUC{iNeu,jTest} = abs(auc(iNeu,jTest)-0.5)+0.5;
                nEachSpot(iNeu,jTest) = numel(auc(iNeu,jTest));
            end
        end
    end
    if all(all(nEachSpot <= 1))
        oneSidedAUC = cell2mat(oneSidedAUC);
    end
else % multiple combos
    for iTest= 1:size(turnTestResults.validTest,1)
        for jNeu = 1:size(turnTestResults.validTest,2)
            for k = 1:numel(turnTestResults.validTest{iTest,jNeu})
                validTest(iTest,jNeu,k) = turnTestResults.validTest{iTest,jNeu}(k);
                if ~validTest(iTest,jNeu,k)
                    oneSidedAUC(iTest,jNeu,k) = NaN;
                else
                    oneSidedAUC(iTest,jNeu,k) = abs(turnTestResults.auc{iTest,jNeu}(k)-0.5)+0.5;
                end
            end
        end
    end
end
end

