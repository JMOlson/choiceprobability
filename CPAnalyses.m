%% Analyzing distribution discriminability - Choice Probabiliy - run with different parameter settings - run once, save for later.
% Analyzing each spatial location L/R discriminability
spaceTurn1Stats = calcRanksumPerievent( 'action', 'space', '1',MPCNeuronStruct_Final,MPCRecStruct_Final, -20, 10, NaN, false,false);
save spaceTurn1Stats spaceTurn1Stats
spaceTurn2Stats = calcRanksumPerievent( 'action', 'space', '2',MPCNeuronStruct_Final,MPCRecStruct_Final, -20, 10, NaN, false,false);
save spaceTurn2Stats spaceTurn2Stats
spaceTurn3Stats = calcRanksumPerievent( 'action', 'space', '3',MPCNeuronStruct_Final,MPCRecStruct_Final, -20, 10, NaN, false,false);
save spaceTurn3Stats spaceTurn3Stats
spaceTurn4Stats = calcRanksumPerievent( 'action', 'space', '4',MPCNeuronStruct_Final,MPCRecStruct_Final, -20, 10, NaN, false,false);
save spaceTurn4Stats spaceTurn4Stats
spaceTurn5Stats = calcRanksumPerievent( 'action', 'space', '5',MPCNeuronStruct_Final,MPCRecStruct_Final, -20, 10, NaN, false,false);
save spaceTurn5Stats spaceTurn5Stats
spaceTurn6Stats = calcRanksumPerievent( 'action', 'space', '6',MPCNeuronStruct_Final,MPCRecStruct_Final, -20, 10, NaN, false,false);
save spaceTurn6Stats spaceTurn6Stats
spaceTurn7Stats = calcRanksumPerievent( 'action', 'space', '7',MPCNeuronStruct_Final,MPCRecStruct_Final, -20, 10, NaN, false,false);
save spaceTurn7Stats spaceTurn7Stats

firstTurnSequenceAnalysis = calcRanksumPerievent( 'action', 'sequence', '1',MPCNeuronStruct_Final,MPCRecStruct_Final, -45, 15, NaN, true, true);
save firstTurnSequenceAnalysis firstTurnSequenceAnalysis
firstTurnTimeAnalysis = calcRanksumPerievent( 'action', 'sequence', '1',MPCNeuronStruct_Final,MPCRecStruct_Final, -2.25, 0.75, NaN, true, true,[],true);
save firstTurnTimeAnalysis firstTurnTimeAnalysis

% Grouping across sequence L/R discriminability
sequenceTurn1Stats = calcRanksumPerievent( 'action', 'sequence', '1',MPCNeuronStruct_Final,MPCRecStruct_Final, -20, 10, NaN, false,false);
save sequenceTurn1Stats sequenceTurn1Stats
sequenceTurn2Stats = calcRanksumPerievent( 'action', 'sequence', '2',MPCNeuronStruct_Final,MPCRecStruct_Final, -20, 10, NaN, false,false);
save sequenceTurn2Stats sequenceTurn2Stats
sequenceTurn3Stats = calcRanksumPerievent( 'action', 'sequence', '3',MPCNeuronStruct_Final,MPCRecStruct_Final, -20, 10, NaN, false,false);
save sequenceTurn3Stats sequenceTurn3Stats

% Grouping choice vs force L/R discriminability
choiceTurnStats = calcRanksumPerievent( 'action', 'choice', '',MPCNeuronStruct_Final,MPCRecStruct_Final, -20, 10, NaN, false,false);
save choiceTurnStats choiceTurnStats

% All turns - downsampled to match turn 1 for 1, not for the other.
allTurnsNODSIncRandomPopStats = calcRanksumPerievent( 'action', 'sequence', '12345',MPCNeuronStruct_Final,MPCRecStruct_Final, -20, 10, NaN, false,false);
save allTurnsNODSIncRandomPopStats allTurnsNODSIncRandomPopStats
allTurnsStats = calcRanksumPerievent( 'action', 'sequence', '12345',MPCNeuronStruct_Final,MPCRecStruct_Final, -20, 10, sum(sequenceTurn1Stats.nEventsUsed,1), false,false);
save allTurnsStats allTurnsStats

% Contexts - mitigating variables in addition to L/R. Analyses conducted
% across L/L and R/R
spaceEffect23Stats = calcRanksumPerievent( 'context', 'space', '23',MPCNeuronStruct_Final,MPCRecStruct_Final, -20, 10, NaN, false,false);
save spaceEffect23Stats spaceEffect23Stats
spaceEffect4567Stats = calcRanksumPerievent( 'context', 'space', '4567',MPCNeuronStruct_Final,MPCRecStruct_Final, -20, 10, NaN, false,false);
save spaceEffect4567Stats spaceEffect4567Stats
sequenceEffectStats = calcRanksumPerievent( 'context', 'sequence', '123',MPCNeuronStruct_Final,MPCRecStruct_Final, -20, 10, NaN, false,false);
save sequenceEffectStats sequenceEffectStats
orientationEffectStats = calcRanksumPerievent( 'context', 'orientation', '',MPCNeuronStruct_Final,MPCRecStruct_Final, -20, 10, NaN, false,false);
save orientationEffectStats orientationEffectStats
pathEffectStats = calcRanksumPerievent( 'context', 'path', '1',MPCNeuronStruct_Final,MPCRecStruct_Final, -20, 10, NaN, false,false);
save pathEffectStats pathEffectStats
choiceEffectStats = calcRanksumPerievent( 'context', 'choice', '',MPCNeuronStruct_Final,MPCRecStruct_Final, -20, 10, NaN, false,false);
save choiceEffectStats choiceEffectStats
