function Results = perieventPlotter( groupType, indexType, turnIndices, neuronsSelected, ...
    TurnRasters, TurnParameters, NeuronStruct, RecStruct,  startPoint, endPoint, plotType)
%perieventPlotter Plot periturn data
%   Detailed explanation goes here
%
%   Input Parameters:
%
%   groupType - determines the categorization for the test
%       'context' - test is for the same action at different turns
%           (i.e. L Forced vs L Choice)
%       'action' - test is for different actions (i.e. L vs R) at the same
%           turn(s).
%
%   contextType - specifies how to interpret the turn indices given
%       'space' - turn indices refer to turn locations in space.
%       'sequence' - turn indices refer to turn location in path sequence
%       'choice' - no turn indices given - indicates groups should separate
%       	choice turns (col 1) vs forced turns (col 2)
%       'path' - differentiate which path an animal is on during a turn
%           common to multiple paths
%       'orientation'
%
%  turnIndices - which turns to use. Code is determined by contextType
%       if space:
%           8                   9
%                4         5
%
%                2    1    3
%
%                6         7
%           A                   B
%       if sequence:
%           There are 5 analogous positions in the sequence of their respective paths.
%           3 outbound           These are 1 2 3 in the order the rat traverses them
%           2 return/inbound     These are 4 5 in the order the rat traverses them
%       if choice:
%           x means choice location, y is forced
%
%           if the 8 path reward system is used, the mapping is:
%
%           if the 4 path reward system is used, the mapping is:
%
%       if path:
%
%
%   plotType - specifies what kind of plot to produce
%       if variability is shown, it is done with shaded color using boundedline function
%
%       'indData' - all individual traversals
%       'meanOnly' - just avg of each group
%       'sem' - means plus std err of the means
%       'std' - means plus std deviation of the samples
%       '95CI' - means plus 95% conf interval of means (bootstrapped)
%       '99CI' -  - means plus 99% conf interval of means (bootstrapped)
%
% Jake Olson April 2017, edited July 2019

%% initialize variables, vectorize inputs if necessary
% noRuns = cellfun(@isempty,LinearRates);
nNeurons = length(TurnParameters);
if ~exist('neuronsSelected','var')
    neuronsSelected = true(nNeurons,1);
elseif isempty(neuronsSelected)
    neuronsSelected = true(nNeurons,1);
elseif length(neuronsSelected) == 1 && islogical(neuronsSelected)
    neuronsSelected = repmat(neuronsSelected,nNeurons,1);
end
if isnumeric(neuronsSelected)
    neuronsToPlotIndices = neuronsSelected;
else
    neuronsToPlotIndices = find(neuronsSelected);
end
nNeuronsToPlot = numel(neuronsToPlotIndices);

%% Data about what is being plotted.
Results.groupType = groupType;
Results.contextType = indexType;
Results.turnsIncluded = turnIndices;

%% Sort out individual turns as lefts and rights
% formGroups is a subfunction included at the bottom of this function

[group1, group2, Results.group1Names, Results.group2Names] = formGroups(groupType, indexType, turnIndices, RecStruct);

Results.group1 = group1;
Results.group2 = group2;

if ~iscell(group1)
    nPairs = 1;
    isACell = 0;
else
    nPairs = size(group1,1);
    isACell = 1;
end

%% Initialize most variables = not for context - those will be cells
Results.nEvents = nan(2,nNeurons,nPairs);

% groups will consist of paths and turns in the path sequence
for iNeuron = 1:nNeuronsToPlot
    neuronIndex = neuronsToPlotIndices(iNeuron);
    iRec = NeuronStruct.recCount(neuronIndex);
    
    % Grab data, store for plots.
    % Grab data, take means, store for tests.
    for iPair = 1:nPairs
        if isACell && strcmp(indexType,'choice')
            group1PathTurn = group1{iPair,iRec};
            group2PathTurn = group2{iPair,iRec};
        elseif isACell
            group1PathTurn = group1{iPair,1};
            group2PathTurn = group2{iPair,1};
        else
            group1PathTurn = group1;
            group2PathTurn = group2;
        end
        
        relevantRows = findRasterRows(TurnParameters{neuronIndex},group1PathTurn);
        group1FRsAll{iPair} = TurnRasters{neuronIndex}(:,relevantRows);
        group1FRsMean{iPair} = nanmean(TurnRasters{neuronIndex}(:,relevantRows),2);

        
        relevantRows = findRasterRows(TurnParameters{neuronIndex},group2PathTurn);
        group2FRsAll{iPair} = TurnRasters{neuronIndex}(:,relevantRows);
        group2FRsMean{iPair} = nanmean(TurnRasters{neuronIndex}(:,relevantRows),2);

    end
    fprintf('Mean FRs for the Lefts: %2.0f\n',cellfun(@(x) max(x),group1FRsMean));
    fprintf('Mean FRs for the Rights: %2.0f\n',cellfun(@(x) max(x),group2FRsMean));
    % create plots
    switch groupType
        case 'action'
            %test each pair to test:
            for iPair = 1:nPairs
                group1FRs = group1FRsAll{iPair};
                group2FRs = group2FRsAll{iPair};
                
                nGroup1 = size(group1FRs,2);
                nGroup2 = size(group2FRs,2);
                Results.nEvents(:,iNeuron,iPair) = [nGroup1;nGroup2];
                
                switch plotType
                    case 'indData'
                        figure;
                        plot([0,0],...
                            [0,max(max(max(group1FRs)),max(max(group2FRs)))],'k-');
                        hold on;
                        plot(startPoint:endPoint,group1FRs,'r');
                        plot(startPoint:endPoint,group2FRs,'g');
                        axis([startPoint,endPoint,0,max(max(max(group1FRs)),max(max(group2FRs)))]);
                    case 'meanOnly'
                        figure;
                        plot(startPoint:endPoint,nanmean(group1FRs,2));
                        hold on;
                        plot(startPoint:endPoint,nanmean(group2FRs,2));
                        axis([startPoint,endPoint,0,max(max(max(group1FRs)),max(max(group2FRs)))]);
                    case 'sem'
                        figure;
                        plot([0,0],[0,max(max(max(group1FRs)),max(max(group1FRs)))],'k-');
                        hold on;
                        boundedline(startPoint:endPoint,nanmean(group1FRs,2),...
                            nanstd(group1FRs,0,2)./sqrt(size(group1FRs,2)),'cmap',[68,114,196]./256,'alpha','transparency',0.2);
                        axis([startPoint,endPoint,0,max(max(max(group1FRs)),max(max(group2FRs)))]);
                        
                        figure;
                        plot([0,0],[0,max(max(max(group1FRs)),max(max(group2FRs)))],'k-');
                        hold on;
                        boundedline(startPoint:endPoint,nanmean(group2FRs,2),...
                            nanstd(group2FRs,0,2)./sqrt(size(group2FRs,2)),'cmap',[192,0,0]./256,'alpha','transparency',0.2);
                        axis([startPoint,endPoint,0,max(max(max(group1FRs)),max(max(group2FRs)))]);
                    case 'std'
                        figure;
                        plot([0,0],[0,max(max(max(group1FRs)),max(max(group1FRs)))],'k-');
                        hold on;
                        boundedline(startPoint:endPoint,nanmean(group1FRs,2),...
                            nanstd(group1FRs,0,2),'cmap',[68,114,196]./256,'alpha','transparency',0.2);
                        axis([startPoint,endPoint,0,max(max(max(group1FRs)),max(max(group2FRs)))]);
                        
                        figure;
                        plot([0,0],[0,max(max(max(group1FRs)),max(max(group2FRs)))],'k-');
                        hold on;
                        boundedline(startPoint:endPoint,nanmean(group2FRs,2),...
                            nanstd(group2FRs,0,2),'cmap',[192,0,0]./256,'alpha','transparency',0.2);
                        axis([startPoint,endPoint,0,max(max(max(group1FRs)),max(max(group2FRs)))]);
                    case '95CI'
                        % bootstrap distributions
                    case '99CI'
                        
                end
            end
        case 'context'
            % test 1 is lefts, test 2 is rights
            sizeGroup1s = cellfun(@(x) size(x,2), group1FRsAll);
            sizeGroup2s = cellfun(@(x) size(x,2), group1FRsAll);
            Results.nEvents(1,iNeuron,:) = sizeGroup1s;
            Results.nEvents(2,iNeuron,:) = sizeGroup2s;
            
            groupData = {group1FRsAll,group2FRsAll};
            maxGraphHeight = 0;
            for iAction = 1:2
                nSubGroups = numel(Results.nEvents(iAction,iNeuron,:));
                groupDataToTest = groupData{iAction};
                colors = distinguishable_colors(nSubGroups,[1,1,1]);
                hFig(iAction) = figure;
                for iGroup = 1:nSubGroups
                    if ~isempty(groupDataToTest{iGroup})
                        switch plotType
                            case 'indData'
                                hold on;
                                plot(startPoint:endPoint,groupDataToTest{iGroup},'-','Color',colors(iGroup,:));
                                maxGraphHeight = max(maxGraphHeight,max(max(groupDataToTest{iGroup})));
                            case 'meanOnly'
                                hold on;
                                plot(startPoint:endPoint,nanmean(groupDataToTest{iGroup},2),'-','Color',colors(iGroup,:));
                                maxGraphHeight = max(maxGraphHeight,max(nanmean(groupDataToTest{iGroup},2)));
                            case 'sem'
                                hold on;
                                boundedline(startPoint:endPoint,nanmean(groupDataToTest{iGroup},2),...
                                    nanstd(groupDataToTest{iGroup},0,2)./sqrt(size(groupDataToTest{iGroup},2)),'-','cmap',colors(iGroup,:));
                                maxVal = nanmean(groupDataToTest{iGroup},2) + nanstd(groupDataToTest{iGroup},0,2)./sqrt(size(groupDataToTest{iGroup},2));
                                maxGraphHeight = max(maxGraphHeight,max(max(maxVal)));
                            case 'std'
                                hold on;
                                boundedline(startPoint:endPoint,nanmean(groupDataToTest{iGroup},2),...
                                    nanstd(groupDataToTest{iGroup},0,2),'-','cmap',colors(iGroup,:));
                                maxVal = nanmean(groupDataToTest{iGroup},2) + nanstd(groupDataToTest{iGroup},0,2);
                                maxGraphHeight = max(maxGraphHeight,max(max(maxVal)));
                            case '95CI'
                                % bootstrap distributions
                            case '99CI'
                        end
                    end
                end
            end
            for iAction = 1:2
                figure(hFig(iAction));
                plot([0,0],[0,maxGraphHeight],'k-');
                axis([startPoint,endPoint,0,maxGraphHeight]);
            end
    end
end
end

function logicalRowVector = findRasterRows(TurnParameters,turnIndices)
%turn indices are identified in [route,progress] pairs
logicalRowVector = false(size(TurnParameters,1),1);
nTurns = size(turnIndices,1);
for iTurn = 1:nTurns
    logicalRowVector(TurnParameters.route == turnIndices(iTurn,1) &...
        TurnParameters.progress == turnIndices(iTurn,2)) = true;
end
end