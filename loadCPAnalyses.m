load([projectFolder,'Data\AnalysisResults_CurrBioRev\spaceTurn1Stats.mat']);
load([projectFolder,'Data\AnalysisResults_CurrBioRev\spaceTurn2Stats.mat']);
load([projectFolder,'Data\AnalysisResults_CurrBioRev\spaceTurn3Stats.mat']);
load([projectFolder,'Data\AnalysisResults_CurrBioRev\spaceTurn4Stats.mat']);
load([projectFolder,'Data\AnalysisResults_CurrBioRev\spaceTurn5Stats.mat']);
load([projectFolder,'Data\AnalysisResults_CurrBioRev\spaceTurn6Stats.mat']);
load([projectFolder,'Data\AnalysisResults_CurrBioRev\spaceTurn7Stats.mat']);

load([projectFolder,'Data\AnalysisResults_CurrBioRev\firstTurnSequenceAnalysis.mat']);
load([projectFolder,'Data\AnalysisResults_CurrBioRev\firstTurnTimeAnalysis.mat']);

load([projectFolder,'Data\AnalysisResults_CurrBioRev\sequenceTurn1Stats.mat']);
load([projectFolder,'Data\AnalysisResults_CurrBioRev\sequenceTurn2Stats.mat']);
load([projectFolder,'Data\AnalysisResults_CurrBioRev\sequenceTurn3Stats.mat']);

load([projectFolder,'Data\AnalysisResults_CurrBioRev\choiceTurnStats.mat']);

load([projectFolder,'Data\AnalysisResults_CurrBioRev\allTurnsNODSIncRandomPopStats.mat']);
load([projectFolder,'Data\AnalysisResults_CurrBioRev\allTurnsStats.mat']);

load([projectFolder,'Data\AnalysisResults_CurrBioRev\spaceEffect23Stats.mat']);
load([projectFolder,'Data\AnalysisResults_CurrBioRev\spaceEffect4567Stats.mat']);
load([projectFolder,'Data\AnalysisResults_CurrBioRev\sequenceEffectStats.mat']);
load([projectFolder,'Data\AnalysisResults_CurrBioRev\orientationEffectStats.mat']);
load([projectFolder,'Data\AnalysisResults_CurrBioRev\pathEffectStats.mat']);
load([projectFolder,'Data\AnalysisResults_CurrBioRev\choiceEffectStats.mat']);

%% Create all of the one-sided AUCs

% Create randomized population.
nNeurons = numel(allTurnsNODSIncRandomPopStats.maxRuns); % NODS is no downsampling of sample size, used all runs.
oneSidedAUCrandomPop = nan(nNeurons,1);
for iNeu = 1:nNeurons
    if ~allTurnsNODSIncRandomPopStats.validTest(iNeu)
        oneSidedAUCrandomPop(iNeu) = NaN;
    else
        oneSidedAUCrandomPop(iNeu) = abs(allTurnsNODSIncRandomPopStats.shuffledAuc(iNeu)-0.5)+0.5;
    end
end
[oneSidedAUCAllNoDS, validTestAllNoDS] = createAUCMat(allTurnsNODSIncRandomPopStats); 
oneSidedAUCAllNoDS(~validTestAllNoDS) = NaN;

% aggregate turn results
allData = [];
allDataLabels = [];
[oneSidedAUCSpace1, validTestSpace1] = createAUCMat(spaceTurn1Stats);
oneSidedAUCSpace1(~validTestSpace1) = NaN;
allData = [allData;oneSidedAUCSpace1];
allDataLabels = [allDataLabels;zeros(numel(oneSidedAUCSpace1),1)+1];
[oneSidedAUCSpace2, validTestSpace2] = createAUCMat(spaceTurn2Stats);
oneSidedAUCSpace2(~validTestSpace2) = NaN;
allData = [allData;oneSidedAUCSpace2];
allDataLabels = [allDataLabels;zeros(numel(oneSidedAUCSpace2),1)+2];
[oneSidedAUCSpace3, validTestSpace3] = createAUCMat(spaceTurn3Stats);
oneSidedAUCSpace3(~validTestSpace3) = NaN;
allData = [allData;oneSidedAUCSpace3];
allDataLabels = [allDataLabels;zeros(numel(oneSidedAUCSpace3),1)+3];
[oneSidedAUCSpace4, validTestSpace4] = createAUCMat(spaceTurn4Stats);
oneSidedAUCSpace4(~validTestSpace4) = NaN;
allData = [allData;oneSidedAUCSpace4];
allDataLabels = [allDataLabels;zeros(numel(oneSidedAUCSpace4),1)+4];
[oneSidedAUCSpace5, validTestSpace5] = createAUCMat(spaceTurn5Stats);
oneSidedAUCSpace5(~validTestSpace5) = NaN;
allData = [allData;oneSidedAUCSpace5];
allDataLabels = [allDataLabels;zeros(numel(oneSidedAUCSpace5),1)+5];
[oneSidedAUCSpace6, validTestSpace6] = createAUCMat(spaceTurn6Stats);
oneSidedAUCSpace6(~validTestSpace6) = NaN;
allData = [allData;oneSidedAUCSpace6];
allDataLabels = [allDataLabels;zeros(numel(oneSidedAUCSpace6),1)+6];
[oneSidedAUCSpace7, validTestSpace7] = createAUCMat(spaceTurn7Stats);
oneSidedAUCSpace7(~validTestSpace7) = NaN;
allData = [allData;oneSidedAUCSpace7];
allDataLabels = [allDataLabels;zeros(numel(oneSidedAUCSpace7),1)+7];

% all individiual internal turns, mean action auc
oneSidedAUCMeanActionAll = nanmean([oneSidedAUCSpace1,...
    oneSidedAUCSpace2,...
    oneSidedAUCSpace3,...
    oneSidedAUCSpace4,...
    oneSidedAUCSpace5,...
    oneSidedAUCSpace6,...
    oneSidedAUCSpace7],2);

% new action analyses - above was for figure 1
[oneSidedAUCSequence1, validTestSequence1] = createAUCMat(sequenceTurn1Stats);
oneSidedAUCSequence1(~validTestSequence1) = NaN;
[oneSidedAUCSequence2, validTestSequence2] = createAUCMat(sequenceTurn2Stats);
oneSidedAUCSequence2(~validTestSequence2) = NaN;
[oneSidedAUCSequence3, validTestSequence3] = createAUCMat(sequenceTurn3Stats);
oneSidedAUCSequence3(~validTestSequence3) = NaN;
[oneSidedAUCChoice, validTestChoice] = createAUCMat(choiceTurnStats);
oneSidedAUCChoice(~validTestChoice) = NaN;

%context analyses aucs
[oneSidedAUCSequenceEffect, validTestSequenceEffect] = createAUCMat(sequenceEffectStats);
oneSidedAUCSequenceEffect(~validTestSequenceEffect) = NaN;
[oneSidedAUCSpaceEffect23, validTestSpaceEffect23] = createAUCMat(spaceEffect23Stats);
oneSidedAUCSpaceEffect23(~validTestSpaceEffect23) = NaN;
[oneSidedAUCSpaceEffect4567, validTestSpaceEffect4567] = createAUCMat(spaceEffect4567Stats);
oneSidedAUCSpaceEffect4567(~validTestSpaceEffect4567) = NaN;
[oneSidedAUCChoiceEffect, validTestChoiceEffect] = createAUCMat(choiceEffectStats);
oneSidedAUCChoiceEffect(~validTestChoiceEffect) = NaN;
[oneSidedAUCPathEffect, validTestPathEffect] = createAUCMat(pathEffectStats);
oneSidedAUCPathEffect(~validTestPathEffect) = NaN;
[oneSidedAUCOrientationEffect, validTestOrientationEffect] = createAUCMat(orientationEffectStats);
oneSidedAUCOrientationEffect(~validTestOrientationEffect) = NaN;

% context conglomeration aucs - hardcoded values here
oneSidedAUCSequenceEffectAll = nanmean(...
    reshape(shiftdim(oneSidedAUCSequenceEffect,1),nNeurons,6),2);
oneSidedAUCSpaceEffectAll = nanmean(...
    [shiftdim(oneSidedAUCSpaceEffect23,1),...
    reshape(shiftdim(oneSidedAUCSpaceEffect4567,1),nNeurons,12)],2);
oneSidedAUCChoiceEffectAll = nanmean(...
    shiftdim(oneSidedAUCChoiceEffect,1),2);
oneSidedAUCOrientationEffectAll = nanmean(...
    reshape(shiftdim(oneSidedAUCOrientationEffect,1),nNeurons,12),2);
oneSidedAUCPathEffectAll = nanmean(...
    reshape(shiftdim(oneSidedAUCPathEffect,1),nNeurons,12),2);

% min of the pairwise put together
oneSidedAUCMinActionAll = nanmin(...
    [oneSidedAUCSpace1,...
    oneSidedAUCSpace2,...
    oneSidedAUCSpace3,...
    oneSidedAUCSpace4,...
    oneSidedAUCSpace5,...
    oneSidedAUCSpace6,...
    oneSidedAUCSpace7],[],2);
oneSidedAUCMinSequenceEffectAll = nanmin(...
    reshape(shiftdim(oneSidedAUCSequenceEffect,1),nNeurons,6),[],2);
oneSidedAUCMinSpaceEffectAll = nanmin(...
    [shiftdim(oneSidedAUCSpaceEffect23,1),...
    reshape(shiftdim(oneSidedAUCSpaceEffect4567,1),nNeurons,12)],[],2);
oneSidedAUCMinChoiceEffectAll = nanmin(...
    shiftdim(oneSidedAUCChoiceEffect,1),[],2);
oneSidedAUCMinOrientationEffectAll = nanmin(...
    reshape(shiftdim(oneSidedAUCOrientationEffect,1),nNeurons,12),[],2);
oneSidedAUCMinPathEffectAll = nanmin(...
    reshape(shiftdim(oneSidedAUCPathEffect,1),nNeurons,12),[],2);

% max of the pairwise put together
oneSidedAUCMaxActionAll = nanmax(...
    [oneSidedAUCSpace1,...
    oneSidedAUCSpace2,...
    oneSidedAUCSpace3,...
    oneSidedAUCSpace4,...
    oneSidedAUCSpace5,...
    oneSidedAUCSpace6,...
    oneSidedAUCSpace7],[],2);
oneSidedAUCMaxSequenceEffectAll = nanmax(...
    reshape(shiftdim(oneSidedAUCSequenceEffect,1),nNeurons,6),[],2);
oneSidedAUCMaxSpaceEffectAll = nanmax(...
    [shiftdim(oneSidedAUCSpaceEffect23,1),...
    reshape(shiftdim(oneSidedAUCSpaceEffect4567,1),nNeurons,12)],[],2);
oneSidedAUCMaxChoiceEffectAll = nanmax(...
    shiftdim(oneSidedAUCChoiceEffect,1),[],2);
oneSidedAUCMaxOrientationEffectAll = nanmax(...
    reshape(shiftdim(oneSidedAUCOrientationEffect,1),nNeurons,12),[],2);
oneSidedAUCMaxPathEffectAll = nanmax(...
    reshape(shiftdim(oneSidedAUCPathEffect,1),nNeurons,12),[],2);

% put together into one variabe in the order of the figure graphs
oneSidedAUCCatAllConds = [oneSidedAUCMeanActionAll,oneSidedAUCSpaceEffectAll,...
    oneSidedAUCSequenceEffectAll, oneSidedAUCOrientationEffectAll,...
    oneSidedAUCPathEffectAll,oneSidedAUCChoiceEffectAll,];
oneSidedAUCCatAllMaxConds = [oneSidedAUCMaxActionAll,oneSidedAUCMaxSpaceEffectAll,...
    oneSidedAUCMaxSequenceEffectAll, oneSidedAUCMaxOrientationEffectAll,...
    oneSidedAUCMaxPathEffectAll,oneSidedAUCMaxChoiceEffectAll];
oneSidedAUCCatAllMinConds = [oneSidedAUCMinActionAll,oneSidedAUCMinSpaceEffectAll,...
    oneSidedAUCMinSequenceEffectAll,oneSidedAUCMinOrientationEffectAll,...
    oneSidedAUCMinPathEffectAll,oneSidedAUCMinChoiceEffectAll];

% sort for finding examples
[vals, origIndices] = sort(oneSidedAUCCatAllConds,'descend');