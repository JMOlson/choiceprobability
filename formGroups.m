function [group1, group2, group1Names, group2Names ] = formGroups(groupType, indexType, turnIndices, RecStruct)
% FORMGROUPS - Helper function to categorize paths and turns correctly.
%   groupType - determines the categorization for the test
%       'context' - test is for the same action at different turns
%           (i.e. L Forced vs L Choice)
%       'action' - test is for different actions (i.e. L vs R) at the same
%           turn(s).
%
%   indexType - specifies how to interpret the turn indices given
%       'space' - turn indices refer to turn locations in space.
%       'sequence' - turn indices refer to turn location in path sequence
%       'choice' - no turn indices given - indicates groups should separate
%       	choice turns (col 1) vs forced turns (col 2)
%       'path' - differentiate which path an animal is on during a turn
%           common to multiple paths
%       'orientation' -
%
%  turnIndices - which turns to use. Code is determined by contextType
%       if space:
%           8                   9
%                4         5
%
%                2    1    3
%
%                6         7
%           A                   B
%       if sequence:
%           There are 5 analogous positions in the sequence of their respective paths.
%           3 outbound           These are 1 2 3 in the order the rat traverses them
%           2 return/inbound     These are 4 5 in the order the rat traverses them
%       if choice:
%           c means choice location, f is forced
%
%           if the 8 path reward system is used, the mapping is:
%           f                   f
%                c         c
%
%                c    c    c
%
%                c         c
%           f                   f
%           if the 4 path reward system is used, the mapping is:
%           f                   f
%                c         c
%
%                f    c    f
%
%                -         -
%           f                   f
%       if path:
%           1-8 for outbound paths, 9-10 for returns. shown in figure
% Jake Olson April 2017, edited July 2019

turnDirections = RecStruct.pathTurnDirections;
turnLocations = RecStruct.turnLocations;
turnChoiceMapping = RecStruct.turnChoiceMapping;
switch groupType
    case 'action'
        group1 = zeros(0,2);
        group2 = zeros(0,2);
        
        switch indexType
            case 'space'
                group1Names = 'lefts';
                group2Names = 'rights';
                for iTurnIndex = 1:numel(turnIndices)
                    [locPaths,locTurns] = find(turnLocations == turnIndices(iTurnIndex));
                    [dirPathsL,dirTurnsL] = find(turnDirections == 'L');
                    group1 = [group1;intersect([locPaths,locTurns],[dirPathsL,dirTurnsL],'rows')];
                    [dirPathsR,dirTurnsR] = find(turnDirections == 'R');
                    group2 = [group2;intersect([locPaths,locTurns],[dirPathsR,dirTurnsR],'rows')];
                end
            case 'sequence'
                group1Names = 'lefts';
                group2Names = 'rights';
                for iTurnIndex = 1:numel(turnIndices)
                    dirPathsL = find(turnDirections(:,str2double(turnIndices(iTurnIndex))) == 'L');
                    group1 = [group1;[dirPathsL,repmat(str2double(turnIndices(iTurnIndex)),numel(dirPathsL),1)]];
                    dirPathsR = find(turnDirections(:,str2double(turnIndices(iTurnIndex))) == 'R');
                    group2 = [group2;[dirPathsR,repmat(str2double(turnIndices(iTurnIndex)),numel(dirPathsR),1)]];
                end
            case 'choice' % first column choice L/R, second column forced
                group1Names{1} = 'lefts - Choice';
                group2Names{1} = 'rights - Choice';
                group1Names{2} = 'lefts - Forced';
                group2Names{2} = 'rights - Forced';
                for iRec = 1:size(turnChoiceMapping,1)
                    tmpLefts = zeros(0,2);
                    tmpRights = zeros(0,2);
                    turnIndices = find(turnChoiceMapping(iRec,:)=='C');
                    for iTurnIndex = 1:numel(turnIndices)
                        thisTurn = num2str(turnIndices(iTurnIndex));
                        switch thisTurn
                            case '10'
                                thisTurn = 'A';
                            case '11'
                                thisTurn = 'B';
                        end
                        [locPaths,locTurns] = find(turnLocations == thisTurn);
                        [dirPathsL,dirTurnsL] = find(turnDirections == 'L');
                        tmpLefts = [tmpLefts;intersect([locPaths,locTurns],[dirPathsL,dirTurnsL],'rows')];
                        [dirPathsR,dirTurnsR] = find(turnDirections == 'R');
                        tmpRights = [tmpRights;intersect([locPaths,locTurns],[dirPathsR,dirTurnsR],'rows')];
                    end
                    group1{1,iRec} = tmpLefts;
                    group2{1,iRec} = tmpRights;
                    tmpLefts = zeros(0,2);
                    tmpRights = zeros(0,2);
                    turnIndices = find(turnChoiceMapping(iRec,:)=='F');
                    for iTurnIndex = 1:numel(turnIndices)
                        thisTurn = num2str(turnIndices(iTurnIndex));
                        switch thisTurn
                            case '10'
                                thisTurn = 'A';
                            case '11'
                                thisTurn = 'B';
                        end
                        [locPaths,locTurns] = find(turnLocations == thisTurn);
                        [dirPathsL,dirTurnsL] = find(turnDirections == 'L');
                        tmpLefts = [tmpLefts;intersect([locPaths,locTurns],[dirPathsL,dirTurnsL],'rows')];
                        [dirPathsR,dirTurnsR] = find(turnDirections == 'R');
                        tmpRights = [tmpRights;intersect([locPaths,locTurns],[dirPathsR,dirTurnsR],'rows')];
                    end
                    group1{2,iRec} = tmpLefts;
                    group2{2,iRec} = tmpRights;
                end
        end
    case 'context' % last dimension is different turn locations
        switch indexType
            case 'space'
                group1Names = 'lefts';
                group2Names = 'rights';
                for iTurnIndex = 1:numel(turnIndices)
                    [locPaths,locTurns] = find(turnLocations == turnIndices(iTurnIndex));
                    [dirPathsL,dirTurnsL] = find(turnDirections == 'L');
                    group1{iTurnIndex,1} = intersect([locPaths,locTurns],[dirPathsL,dirTurnsL],'rows');
                    [dirPathsR,dirTurnsR] = find(turnDirections == 'R');
                    group2{iTurnIndex,1} = intersect([locPaths,locTurns],[dirPathsR,dirTurnsR],'rows');
                end
            case {'sequence', 'path'}
                group1Names = 'lefts';
                group2Names = 'rights';
                for iTurnIndex = 1:numel(turnIndices)
                    dirPathsL = find(turnDirections(:,str2double(turnIndices(iTurnIndex))) == 'L');
                    group1{iTurnIndex,1} = [dirPathsL,repmat(str2double(turnIndices(iTurnIndex)),numel(dirPathsL),1)];
                    dirPathsR = find(turnDirections(:,str2double(turnIndices(iTurnIndex))) == 'R');
                    group2{iTurnIndex,1} = [dirPathsR,repmat(str2double(turnIndices(iTurnIndex)),numel(dirPathsR),1)];
                end
                if strcmp(indexType, 'path')
                    for iRow = size(group1{1},1):-1:1
                        group1{iRow,1} = group1{1}(iRow,:);
                    end
                    for iRow = size(group2{1},1):-1:1
                        group2{iRow,1} = group2{1}(iRow,:);
                    end
                end
            case 'choice' % firs column choice L/R, second column forced
                group1Names{1} = 'lefts - Choice';
                group2Names{1} = 'rights - Choice';
                group1Names{2} = 'lefts - Forced';
                group2Names{2} = 'rights - Forced';
                for iRec = 1:size(turnChoiceMapping,1)
                    tmpLefts = zeros(0,2);
                    tmpRights = zeros(0,2);
                    turnIndices = find(turnChoiceMapping(iRec,:)=='C');
                    for iTurnIndex = 1:numel(turnIndices)
                        thisTurn = num2str(turnIndices(iTurnIndex));
                        switch thisTurn
                            case '10'
                                thisTurn = 'A';
                            case '11'
                                thisTurn = 'B';
                        end
                        [locPaths,locTurns] = find(turnLocations == thisTurn);
                        [dirPathsL,dirTurnsL] = find(turnDirections == 'L');
                        tmpLefts = [tmpLefts;intersect([locPaths,locTurns],[dirPathsL,dirTurnsL],'rows')];
                        [dirPathsR,dirTurnsR] = find(turnDirections == 'R');
                        tmpRights = [tmpRights;intersect([locPaths,locTurns],[dirPathsR,dirTurnsR],'rows')];
                    end
                    group1{1,iRec} = tmpLefts;
                    group2{1,iRec} = tmpRights;
                    
                    tmpLefts = zeros(0,2);
                    tmpRights = zeros(0,2);
                    turnIndices = find(turnChoiceMapping(iRec,:)=='F');
                    for iTurnIndex = 1:numel(turnIndices)
                        thisTurn = num2str(turnIndices(iTurnIndex));
                        switch thisTurn
                            case '10'
                                thisTurn = 'A';
                            case '11'
                                thisTurn = 'B';
                        end
                        [locPaths,locTurns] = find(turnLocations == thisTurn);
                        [dirPathsL,dirTurnsL] = find(turnDirections == 'L');
                        tmpLefts = [tmpLefts;intersect([locPaths,locTurns],[dirPathsL,dirTurnsL],'rows')];
                        [dirPathsR,dirTurnsR] = find(turnDirections == 'R');
                        tmpRights = [tmpRights;intersect([locPaths,locTurns],[dirPathsR,dirTurnsR],'rows')];
                    end
                    group1{2,iRec} = tmpLefts;
                    group2{2,iRec} = tmpRights;
                end
            case 'orientation'
                group1Names = 'lefts';
                group2Names = 'rights';
                orientationCodes = ['SES  ';...
                    'SES  ';...
                    'SWS  ';...
                    'SWS  ';...
                    'SEN  ';...
                    'SEN  ';...
                    'SWN  ';...
                    'SWN  ';...
                    '   EN';...
                    '   WN']; %Directions of the turns for the TT maze. NSEW are relative not absolute.
                orientations = 'NESW';
                for iOrientationIndex = 1:numel(orientations)
                    iOrientation = orientations(iOrientationIndex);
                    [locPaths,locTurns] = find(orientationCodes == iOrientation);
                    [dirPathsL,dirTurnsL] = find(turnDirections == 'L');
                    group1{iOrientationIndex,1} = intersect([locPaths,locTurns],[dirPathsL,dirTurnsL],'rows');
                    [dirPathsR,dirTurnsR] = find(turnDirections == 'R');
                    group2{iOrientationIndex,1} = intersect([locPaths,locTurns],[dirPathsR,dirTurnsR],'rows');
                end
        end
end
end